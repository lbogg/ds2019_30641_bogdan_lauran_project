package com.med.med.models.data;

import com.med.med.models.Medication;
import com.med.med.models.UserActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface UserActivityDao extends JpaRepository<UserActivity,Long> {
}
