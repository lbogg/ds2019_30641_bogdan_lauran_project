package com.med.med.controller;

import com.med.med.services.ActivityService;
import io.spring.guides.gs_producing_web_service.Activity;
import io.spring.guides.gs_producing_web_service.GetActivityRequest;
import io.spring.guides.gs_producing_web_service.GetActivityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
@Endpoint
public class ActivityEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    @Autowired
    private ActivityService activityService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivityRequest")
    @ResponsePayload
    public GetActivityResponse getActivityResponse(@RequestPayload GetActivityRequest request) {
        GetActivityResponse response = new GetActivityResponse();
        response.setActivity(activityService.fetchActivities());
        return response;
    }
}
