package com.med.med.controller;

import com.med.med.models.Medication;
import com.med.med.services.MedService;
import com.med.med.utils.MedicationDto;
import com.med.med.models.data.MedicationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/medication")
public class MedicationController {
    @Autowired
    MedicationDao medicationDao;

    @Autowired
    private MedService medService;

    @GetMapping
    public List<Medication> displayMeds(){
        return medicationDao.findAll().parallelStream().peek(medication -> {
            medication.getPatient().setMedicalRecord(null);
        }).collect(Collectors.toList());
    }

    @GetMapping("/my-medication")
    public List<Medication> displayMyMedication(){
        String username = ((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return medicationDao.findAll().parallelStream().peek(medication -> {
            medication.getPatient().setMedicalRecord(null);
        }).filter(med -> med.getPatient().getUsername().equals(username)).collect(Collectors.toList());
    }

    @PostMapping
    public Medication createMed(@RequestBody MedicationDto medicationDto){
        Medication medication = medService.makeMedication(medicationDto);
        medication.getPatient().setMedicalRecord(null);
        return medication;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){medService.deleteMed(id);}

}
