package com.med.med.utils;

import com.med.med.models.UserActivity;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class ActivityReader {
    public List<UserActivity> doRead() {
        List<UserActivity> out = new ArrayList<>();
        Scanner scanner = getScanner();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] data = line.split("\t\t");

            UserActivity userActivity = new UserActivity();
            userActivity.setStart(data[0]);
            userActivity.setEnd(data[1]);
            userActivity.setDescription(data[2]);

            out.add(userActivity);
        }

        return out;
    }

    private Scanner getScanner() {
        try {
            File file;

            if (Files.exists(Paths.get("./activity.txt"))) {
                file = new File("./activity.txt");
            } else {
                file = new File(this.getClass().getClassLoader().getResource("activity.txt").getFile());
            }

            return new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Could not load activity file.", e);
        }
    }
}
