package com.med.med.services;

import com.med.med.models.data.UserActivityDao;
import io.spring.guides.gs_producing_web_service.Activity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityService {
    private final UserActivityDao userActivityDao;

    public ActivityService(UserActivityDao userActivityDao) {
        this.userActivityDao = userActivityDao;
    }

    public Activity fetchActivities() {
        List<Activity> activities = userActivityDao.findAll().parallelStream().map(userActivity -> {
            Activity activity = new Activity();
            activity.setName(userActivity.getUser().getName());
            activity.setId((int)userActivity.getUser().getId());
            activity.setStart(userActivity.getStart());
            activity.setDescription(userActivity.getDescription());
            activity.setEnd(userActivity.getEnd());
            return activity;
        }).collect(Collectors.toList());


        for(int i = 0; i < activities.size() - 1; i++) {
            activities.get(i).setActivity(activities.get(i + 1));
        }

        return activities.get(0);
    }
}
