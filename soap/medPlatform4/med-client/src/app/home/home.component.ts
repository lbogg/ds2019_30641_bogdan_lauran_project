import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {SoapService} from "../services/soap.service";
import {AuthService} from "../services/auth.service";
import {User} from "../models/user.model";

@Component({
  selector: 'app-home',
  template: `
      <app-navbar></app-navbar>

      <div *ngIf="user && user.role.toLowerCase() === 'doctor'">
        <h3>Patient activity list</h3>
        <table class="table">
          <thead>
          <th>#</th>
          <th>Name</th>
          <th>Activity</th>
          <th>Started At</th>
          <th>Finished At</th>
          </thead>
          <tbody>
          <tr *ngFor="let activity of activities | async">
            <td>{{activity.id }}</td>
            <td>{{activity.name }}</td>
            <td>{{activity.description }}</td>
            <td>{{activity.start | date: 'medium' }}</td>
            <td>{{activity.end  | date:'medium'}}</td>
          </tr>
          </tbody>
        </table>
      </div>
  `,
  styleUrls: [`./home.component.css`]
})
export class HomeComponent implements OnInit {
  activities: Promise<any[]>;
  user: User;

  constructor(private soap: SoapService, private auth: AuthService) {}

  ngOnInit() {
    this.activities = this.soap.getActivities();
    this.user = JSON.parse(localStorage.getItem('user'));
  }
}
