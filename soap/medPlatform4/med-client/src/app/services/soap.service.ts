import {Injectable} from '@angular/core';
import * as fastXmlParser from 'fast-xml-parser';

@Injectable({
  providedIn: 'root'
})
export class SoapService {
  constructor() { }

  async request<T>(data: string): Promise<T> {
    const result: string = await this.soap(data);
    let obj = fastXmlParser.parse(result);
    obj = JSON.stringify(obj).replace(/ns2:/g, '');
    return Object.values(JSON.parse(obj))[0] as T;
  }

  private async soap(xml): Promise<string> {
    return (window as any).soapRequest({
      url: 'http://localhost:4200/api/ws',
      headers: {
        'Content-Type': 'text/xml;charset=UTF-8',
      },
      xml
    }).then(x => x.response.body.replace('<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body>', '')
    ).then(x => x.replace('</SOAP-ENV:Body></SOAP-ENV:Envelope>', ''))
  }

  private toArray(x) {

    const activity = x.activity;

    if (x.activity) {
      delete x.activity;
      return [ x, ...this.toArray(activity) ];
    }



    return [x];
  }

  async getActivities(): Promise<{id: number, description: string, start: string, end: string, name: string}[]> {
    return this.request(`
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:gs="http://spring.io/guides/gs-producing-web-service">
     <soapenv:Header/>
     <soapenv:Body>
        <gs:getActivityRequest/>
     </soapenv:Body>
  </soapenv:Envelope>
    `).then(data => this.toArray(data).slice(1));
  }
}

// <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
// xmlns:gs="http://spring.io/guides/gs-producing-web-service">
//   <soapenv:Header/>
// <soapenv:Body>
// <gs:getCountryRequest>
// <gs:name>Spain</gs:name>
// </gs:getCountryRequest>
// </soapenv:Body>
// </soapenv:Envelope>
