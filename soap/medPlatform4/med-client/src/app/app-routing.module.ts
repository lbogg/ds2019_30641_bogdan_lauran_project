import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {PatientsListComponent} from "./patients-list/patients-list.component";


const routes: Routes = [
  //{path: 'login', canActivate: [ AnonymousGuard ], component: LoginComponent}
  //{ path: '', pathMatch: 'full', redirectTo: '/patients'},
  { path: '', pathMatch: 'full', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'patients', component: PatientsListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
