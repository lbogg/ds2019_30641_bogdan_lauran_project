import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {User} from "../models/user.model";
import {AuthService} from "../services/auth.service";
import {map} from "rxjs/internal/operators";

@Component({
  selector: 'app-patients-list',
  template: `
   <app-navbar></app-navbar>
   <div class="container">
    <!-- <button type="button" class="btn btn-info">Button</button>-->
     <table class="table table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Role</th>
          <th>Username</th>
          <th></th>
          <th></th>
        </tr>
        </thead>
        <tbody *ngIf="users$ | async as users">
            <tr *ngFor="let user of users">
              <td>{{ user.id }}</td>
              <td>{{ user.name }}</td>
              <td>{{ user.role }}</td>
              <td>{{ user.username }}</td>
              <td (click)="onDelete(user.id)"> <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button></td>
              <td *ngIf="user.role === 'PATIENT'" (click)="onAddMedication(user.id)">Add Medication</td>
            </tr>
        </tbody>
      </table>
    </div>
  `,
  styleUrls: ['./patients-list.component.css']
})
export class PatientsListComponent implements OnInit {
  users$: Observable<User[]>;
  logged: User;

  constructor(private http: HttpClient, private auth: AuthService) { }

  ngOnInit() {
    this.logged = JSON.parse(localStorage.getItem('user')) as User;
    this.users$ = this.http.get<User[]>('/api/users').pipe(
      map(users => users.filter(user => user.username !== this.logged.username))
    );
  }

  onDelete(id) {
    if(!confirm('Are you sure you want to delete this user?')) {
      return;
    }

    this.http.delete(`/api/users/` + id).subscribe(() => this.ngOnInit());
  }

}
