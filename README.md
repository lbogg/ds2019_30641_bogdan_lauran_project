# README #

Integrated Medical Monitoring Platform for Homecare assistance

### What is this repository for? ###

	Run Docker
	docker run --all

### How do I get set up? ###

* git init
* git remote add origin the link of the project from Bitbucket
* git pull origin master
* Database configuration

* starting the database server and loading the database schema
* installing all necessary libraries and running the admin service
* installing all necessary libraries and running the client service
* opening the client application, and loading the WSDL files from the two services, at the location published by them
* running the client application and interacting with it on the webpage


### Description ###

* Online Medication Platform: an online medication platform build using RESTful
services and JavaScript-based client applications
* Pill-Box Medication Notification: a medication intake notification system built
using RPC-based technologies
* Sensor Monitoring System and Real-Time Notification: a monitoring system for
sensor data acquisition built with message-oriented-middleware and web sockets for
asynchronous notifications
* Professional Medication Services: a set of SOA web services for a doctor
application

### Dockerfile example ###
FROM openjdk:11.0-jre-stretch
RUN mkdir build
COPY ./target/medPlatform-1.0-SNAPSHOT.jar /build/server.jar
WORKDIR /build
CMD sleep 2 && java --illegal-access=permit -jar ./server.jar com.ProjectApplication

### Docker setup ###
version: '3.3'

services:
    server:
        build:
            context: medPlatform4
            dockerfile: Dockerfile
        ports:
            - 8080:8080
        depends_on: 
            - db
        expose: 
            - '8080'
    
    client:
        build:
            context: medPlatform4/med-client
            dockerfile: Dockerfile
        ports:
            - 4200:4200
        depends_on: 
            - server
        expose: 
            - '4200'

    db:
        image: mysql:5.7
        restart: always
        environment:
            MYSQL_DATABASE: 'medPlatform'
            MYSQL_USER: 'user'
            MYSQL_PASSWORD: 'root'
            MYSQL_ROOT_PASSWORD: 'root'
        ports:
            - '3306:3306'
        expose:
            - '3306'
        volumes:
            - my-db:/var/lib/mysql

volumes:
    my-db:

