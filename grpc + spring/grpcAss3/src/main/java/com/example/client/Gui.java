package com.example.client;

import com.example.grpc.*;
import io.grpc.ManagedChannel;

import javax.swing.*;
import java.awt.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import static java.lang.Thread.sleep;

public class Gui{

    private JFrame frame = new JFrame("Pill dispenser");
    private JLabel timeLabel;

    private ManagedChannel managedChannel;
    private UserServiceGrpc.UserServiceBlockingStub messageStub;

    private DisplayResponse response;
    ClientServices clientServices;
    public Gui(ManagedChannel managedChannel, UserServiceGrpc.UserServiceBlockingStub messageStub) {


        this.managedChannel = managedChannel;
        this.messageStub = messageStub;

        frame.setLayout(null);
        frame.getContentPane().setLayout(new GridLayout(0,1));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,600);

        timeLabel = new JLabel("");
        timeLabel.setBounds(30, 30, 150, 30);
        this.clientServices = new ClientServices(managedChannel, messageStub);
        //Thread separat pentru afisarea timerului
        Thread thread = new Thread(() -> {
            try {
                for(;;) {
                    LocalTime time = LocalTime.now();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                    System.out.println("TIMP:" + time.format(formatter));
                    timeLabel.setText("Current time: " + time.format(formatter));
                    checkHourlyForMedicines(this.response);
                    sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
        frame.getContentPane().add(timeLabel);

        frame.setVisible(true);
    }

    public void loadWithData(DisplayResponse displayResponse) {

        for(MedGrpc medPlanGrpc: displayResponse.getMedPlanListList()) {
            addMedToGUI(medPlanGrpc);
        }
        this.response = displayResponse;
    }

    public void checkHourlyForMedicines(DisplayResponse displayResponse) {
        System.out.println("MED NOT TAKEN");
        Thread thread = new Thread(() -> {
            try {
                for (MedGrpc medGrpc : displayResponse.getMedPlanListList()) {
                        if (!clientServices.checkMed(medGrpc)) {
                            // mark as not taken
                            noticeMedNotTaken();
                        }
                }
                sleep(1000); //30 de min
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }


    public void addMedToGUI(MedGrpc medGrpc) {
        System.out.println("Adding med to plan");

        JPanel medPanel = new JPanel();
        medPanel.setLayout(new FlowLayout());

        JLabel medTitle = new JLabel(medGrpc.getName());
        JLabel intakeInterval = new JLabel(medGrpc.getStart() + " " + medGrpc.getEnd());
        JButton takeButton = new JButton("Take");

        takeButton.addActionListener(e -> {
            System.out.println("TAKE OR NOT?" + clientServices.checkMed(medGrpc));
            if(clientServices.checkMed(medGrpc)) {
                intakeInterval.setText("- taken");
                takeButton.setVisible(false);
                noticeMedTaken();
            } else {
                JOptionPane.showMessageDialog(frame, "Medicine cannot be taken now");
                noticeMedNotTaken();
            }
        }
        );

        medPanel.add(medTitle);
        medPanel.add(intakeInterval);
        medPanel.add(takeButton);
        frame.getContentPane().add(medPanel);
    }

    public void noticeMedTaken(){
        MedStatus status = MedStatus.newBuilder().setStatus("taken").build();
        StatusResponse statusResponse = messageStub.notice(status);
        System.out.println(statusResponse.getRsp());
    }

    public void noticeMedNotTaken(){
        MedStatus status = MedStatus.newBuilder().setStatus("nottaken").build();
        StatusResponse statusResponse = messageStub.notice(status);
        System.out.println(statusResponse.getRsp());
    }

}
