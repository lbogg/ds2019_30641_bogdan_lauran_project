package com.example.client;

import com.example.grpc.DisplayResponse;
import com.example.grpc.MedGrpc;

import com.example.grpc.UserServiceGrpc;
import io.grpc.ManagedChannel;

import java.sql.Time;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ClientServices {

    private ManagedChannel managedChannel;
    private UserServiceGrpc.UserServiceBlockingStub stub;

    public ClientServices(ManagedChannel managedChannel, UserServiceGrpc.UserServiceBlockingStub stub){
        this.managedChannel = managedChannel;
        this.stub = stub;
    }

    public Boolean checkMed(MedGrpc medPlan) {
        String start = medPlan.getStart();
        String end = medPlan.getEnd();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime startTime = LocalTime.parse(start, formatter);
        LocalTime endTime = LocalTime.parse(end, formatter);
        LocalTime timeNow = LocalTime.now();
        boolean taken = false;
        if (startTime.isAfter(endTime)) {
            //pe timp de zi
            if (timeNow.isBefore(endTime) || timeNow.isAfter(startTime)) {
                System.out.println("TRUE");
            } else {
                taken = false;
            }
        } else {
            if (timeNow.isBefore(endTime) && timeNow.isAfter(startTime)) {
                System.out.println("TRUE");
            } else {
                taken = false;
            }
        }
        return taken;
    }
}
