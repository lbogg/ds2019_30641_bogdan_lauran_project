package com.example.client;

import com.example.grpc.DisplayRequest;
import com.example.grpc.DisplayResponse;
import com.example.grpc.UserServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;


public class ClientApp {

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("server",8081).usePlaintext().build();
        UserServiceGrpc.UserServiceBlockingStub messageStub = UserServiceGrpc.newBlockingStub(channel);

            DisplayRequest request = DisplayRequest.newBuilder()
                    .setGreeting("get")
                    .build();
            DisplayResponse response = messageStub.planDisplay(request);
            System.out.println("RESPONSE: "+ response);
            Gui gui = new Gui(channel, messageStub);
            gui.loadWithData(response);
        }

}
