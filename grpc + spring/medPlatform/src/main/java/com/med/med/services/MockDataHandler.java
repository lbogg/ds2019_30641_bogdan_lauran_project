package com.med.med.services;

import com.med.med.models.Role;
import com.med.med.models.User;
import com.med.med.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

@Component
public class MockDataHandler {
    @Autowired
    private UserDao userDao;

    @PostConstruct
    void init() {
        User doctor = new User();
        doctor.setName("Doctor Doctorescu");
        doctor.setPassword("parola");
        doctor.setUsername("doctor");
        doctor.setGender("Male");
        doctor.setRole(Role.DOCTOR);
        doctor.setAddress("Eroilor 7");
        doctor.setBirthDate(LocalDate.now().minusYears(45));
        userDao.save(doctor);

        User patient1 = new User();
        patient1.setName("Patient Patientescu 1");
        patient1.setPassword("parola");
        patient1.setUsername("patient1");
        patient1.setGender("Male");
        patient1.setRole(Role.PATIENT);
        patient1.setAddress("Eroilor 8");
        patient1.setBirthDate(LocalDate.now().minusYears(41));
        userDao.save(patient1);

        User patient2 = new User();
        patient2.setName("Patient Patientescu 2");
        patient2.setPassword("parola");
        patient2.setUsername("patient2");
        patient2.setGender("Female");
        patient2.setRole(Role.PATIENT);
        patient2.setAddress("Eroilor 9");
        patient2.setBirthDate(LocalDate.now().minusYears(40));
        userDao.save(patient2);

        User caretaker1 = new User();
        caretaker1.setName("Mircea");
        caretaker1.setPassword("parola");
        caretaker1.setUsername("caretaker1");
        caretaker1.setGender("Male");
        caretaker1.setRole(Role.CAREGIVER);
        caretaker1.setAddress("Eroilor 10");
        caretaker1.setBirthDate(LocalDate.now().minusYears(46));
        userDao.save(caretaker1);

        User caretaker2 = new User();
        caretaker2.setName("Marinela");
        caretaker2.setPassword("parola");
        caretaker2.setUsername("caretaker2");
        caretaker2.setGender("Female");
        caretaker2.setRole(Role.CAREGIVER);
        caretaker2.setAddress("Eroilor 10");
        caretaker2.setBirthDate(LocalDate.now().minusYears(42));
        userDao.save(caretaker2);
    }
}
