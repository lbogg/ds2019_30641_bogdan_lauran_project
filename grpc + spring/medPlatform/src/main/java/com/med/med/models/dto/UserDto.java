package com.med.med.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.med.med.models.Role;

import java.time.LocalDate;
import java.util.List;

public class UserDto {
    public long id;
    public String username;
    @JsonIgnore
    public String password;
    public String name;
    public Role role;
    public LocalDate birthDate;
    public String gender;
    public String address;
    public List<Long> medicalRecord;
    public List<UserDto> patients;
}
