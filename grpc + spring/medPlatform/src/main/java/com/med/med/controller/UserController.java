package com.med.med.controller;

import com.med.med.models.data.MedicationDao;
import com.med.med.models.dto.UserDto;
import com.med.med.services.MedService;
import com.med.med.models.Role;
import com.med.med.models.User;
import com.med.med.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserDao userDao;
    @Autowired
    private MedicationDao medicationDao;
    @Autowired
    private MedService medService;

    @GetMapping
    public List<User> displayUsers(){
        return userDao.findAll();
    }

    @GetMapping("/patients")
    public List<User> displayPatients(){return userDao.findAllByRole(Role.PATIENT);}

    @GetMapping("/caretakers")
    public List<User> displayCaretakers(){return userDao.findAllByRole(Role.CAREGIVER);}

    @GetMapping("/profile")
    public User profile() {
        return userDao.findByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).orElseThrow(EntityNotFoundException::new);
    }

    @PostMapping("/patients")
    public void create(@RequestBody UserDto user) {
        User u = new User();
        u.setRole(Role.PATIENT);
        u.setUsername(user.username);
        u.setPassword(user.password);
        u.setName(user.name);
        u.setGender(user.gender);
        u.setAddress(user.address);
        u.setBirthDate(user.birthDate);
//        u.setm(user.getBirthDate());
        userDao.save(u);
    }

    @PostMapping("/caretakers")
    public void createCaretaker(@RequestBody UserDto user) {
        User u = new User();
        u.setRole(Role.CAREGIVER);
        u.setUsername(user.username);
        u.setPassword(user.password);
        u.setName(user.name);
        u.setGender(user.gender);
        u.setAddress(user.address);
        u.setBirthDate(user.birthDate);
//        u.setm(user.getBirthDate());
        userDao.save(u);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        medService.deleteUser(id);
    }


}
