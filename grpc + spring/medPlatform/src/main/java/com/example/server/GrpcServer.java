package com.example.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class GrpcServer {

    public void main(String[] args) throws IOException, InterruptedException {

        Server server = ServerBuilder.forPort(8081).addService(new UserService()).build();

        server.start();
        System.out.println("Server started on port " + server.getPort());
//        server.awaitTermination();
    }
}
