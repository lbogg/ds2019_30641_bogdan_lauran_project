import {MedicationPlan} from './medication.model';

export interface User {
  id: number;
  username: string;
  password?: string;
  name: string;
  gender: string;
  address: string;
  birthDate: string;
  role: 'DOCTOR' | 'CARETAKER' | 'PATIENT';
}

export interface Patient extends User {
  role: 'PATIENT';
  medicalRecord: string;
  medicationPlans: MedicationPlan[];
}

export interface Caretaker extends User {
  role: 'CARETAKER';
  patients: Patient[];
}

export interface Doctor extends User {
  role: 'DOCTOR';
}
